import Vue from 'vue'
import VueRouter from 'vue-router'
import Chat from './components/Chat'
import Test from './components/test'
Vue.use(VueRouter)

const routes = [
    { path: '/chat', component: Chat },
    { path: '/test', component: Test }
]

const router = new VueRouter({
    routes // short for `routes: routes`
})
export default router;